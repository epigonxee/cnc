<?php
// Text
$_['text_items']     = '%s';
$_['tcart']     = 'Your shopping cart is empty!';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';