
/* Slider Loader*/
$(window).load(function myFunction() {
    $(".s-panel .loader").removeClass("wrloader");
});
// -----

/* dropdown effect of account */
$(document).ready(function () {
    if ($(window).width() <= 767) {
        $('#wishlist-total').appendTo('.account');
        $('.compro').appendTo('.account');
        $('#form-currency').appendTo('.account');
        $('#form-language').appendTo('.account');
        $('#account').appendTo('.bbb');
        $('#cart').appendTo('.bbb');
        $('#menu').appendTo('.logos');
        $('#column-left').appendTo('#content');
        $('.catfilter').appendTo('.mrefine');

        // $('#compro').appendTo('.account');

        $('.dropdown button.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });

        $('.dropdown a.account').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    }
});
/* dropdown */
// rating
$(document).ready(function () {
    $("#ratep,#ratepr").click(function() {
        $('html, body').animate({
            scrollTop: $(".product-tab").offset().top }, 1800);
    });
});
// rating end
// search dropdwon
$(document).ready(function () {
    $(".position-static .search-down").click(function(){
        $('.position-static .searchtg').parent().toggleClass('active');
        $('.position-static .searchtg').toggle('slow',function(){});
        $('.position-static .ui-autocomplete-input').attr('autofocus','autofocus').focus()});
});
// end

/* responsive menu */
function openNav() {
    $('body').addClass("active");
    document.getElementById("mySidenav").style.width = "250px";
    jquery('#mySidenav').addClass("dblock");
}
function closeNav() {
    $('body').removeClass("active");
    document.getElementById("mySidenav").style.width = "0";
    jquery('#mySidenav').addClass("dnone");
}

/* sticky header */
// header top
$(document).ready(function(){
    if ($(document).width() >= 768){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 165) {
                $('.header-top').addClass('fixed fadeInDown animated');
            } else {
                $('.header-top').removeClass('fixed fadeInDown animated');
            }
        });
    };
});


//go to top
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#scroll').fadeIn();
        } else {
            $('#scroll').fadeOut();
        }
    });
    $('#scroll').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
});

$(document).ready(function(){
    if ($(window).width() >= 768){
        var count_block = $('.moremenu').length;
        var number_blocks = 10;
        //console.log(count_block);
        if(count_block < number_blocks){
            return false;
        }
        else {
            $('.moremenu').each(function(i,n){
                $('moremenu').addClass(i);
                if(i == number_blocks) {
                    $(this).append('<li class="view_more my-menu"><a class="dropdown-toggle">More Categories</a></li>');
                }
                if(i> number_blocks) {
                    $(this).addClass('wr_hide_menu').hide();
                }
            });
            $('.view_more').click(function() {
                $(this).toggleClass('active');
                $('.wr_hide_menu').slideToggle();
            });
        }
    }
});

$(document).ready(function(){
    if (($(window).width() >= 768) && ($(window).width() <= 991)){
        var count_block = $('.moremenu').length;
        var number_blocks = 4;
        //console.log(count_block);
        if(count_block < number_blocks){
            return false;
        }
        else {
            $('.moremenu').each(function(i,n){
                $('moremenu').addClass(i);
                if(i == number_blocks) {
                    $(this).append('<li class="view_more my-menu"><a class="dropdown-toggle">More Categories</a></li>');
                }
                if(i> number_blocks) {
                    $(this).addClass('wr_hide_menu').hide();
                }
            });
            $('.view_more').click(function() {
                $(this).toggleClass('active');
                $('.wr_hide_menu').slideToggle();
            });
        }
    }
});



$(document).ready(function(){
    var count_block = $('ul .webTab').length;
    var number_blocks = 10;
    //console.log(count_block);
    if(count_block < number_blocks){
        return false;
    }
    else {
        $('.webTab').each(function(i,n){
            $('webTab').addClass(i);
            if(i == number_blocks) {
                $(this).append('<li class="view_morec"><a class="dropdown-item">More Categories</a></li>');
            }
            if(i> number_blocks) {
                $(this).addClass('wr_hide_menuc').hide();
                //$(this).hide();
            }              //$('.webTab').hide();
        });
        $('.view_morec').click(function() {
            $(this).toggleClass('active');
            $('.wr_hide_menuc').slideToggle();
        });
    }
});