<?php if (count($currencies) == 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">
    <div class="btn-group">
        <button class="btn btn-link dropdown-toggle test" data-toggle="dropdown">
            <span class=""></span>
            <?php echo $text_currency; ?>
            <i class="fa fa-angle-down"></i>
        </button>
        <ul class="dropdown-menu">
            <?php foreach ($currencies as $currency) { ?>
                <?php if ($currency['code'] == $currency_code) { ?>
                    <?php if ($currency['symbol_left']) { ?>
                        <li>
                            <a class="currency-select btn btn-link btn-block" title="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left'] . ' ' . $currency['title']; ?></a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a class="currency-select btn btn-link btn-block" title="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right'] . ' ' . $currency['title']; ?></a>
                        </li>
                    <?php } ?>
                <?php } else { ?>
                    <?php if ($currency['symbol_left']) { ?>
                        <li>
                            <button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left'] . ' ' . $currency['title']; ?></button>
                        </li>
                    <?php } else { ?>
                        <li>
                            <button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left'] . ' ' . $currency['title']; ?></button>
                        </li>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <li>
                <button class="currency-select btn btn-link btn-block" type="button" name="GBP">£ Pound Sterling</button>
            </li>
            <li>
                <button class="currency-select btn btn-link btn-block" type="button" name="USD">$ US Dollar</button>
            </li>
        </ul>
    </div>
    <input type="hidden" name="code" value="" />
    <input type="hidden" name="redirect" value="/index.php?route=common/home" />
</form>
<?php } ?>
