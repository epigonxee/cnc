<?php if (count($languages) == 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language">
    <div class="btn-group acc">
        <button class="btn btn-link dropdown-toggle test" data-toggle="dropdown">
            <span class="">
                <?php echo $text_language; ?>
            </span>
            <i class="fa fa-angle-down"></i>
        </button>
        <ul class="dropdown-menu">
            <?php foreach ($languages as $language) { ?>
                <li>
                    <button class="btn btn-link btn-block language-select" type="button" name="<?php echo $language['code']; ?>">
                        <img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" />
                        <?php echo $language['name']; ?>
                    </button>
                </li>
            <?php } ?>
            <li>
                <button class="btn btn-link btn-block language-select" type="button" name="ar"><img src="image/flags/ar.png" alt="arabic" title="arabic" />arabic</button>
            </li>
        </ul>
    </div>
    <input type="hidden" name="code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
<?php } ?>
