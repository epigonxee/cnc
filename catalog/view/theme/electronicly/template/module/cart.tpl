<div id="cart" class="btn-group btn-block">
    <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
        <div class="cimg">
            <svg width="22px" height="21px">
                <use xlink:href="#hcart"></use>
            </svg>
        </div>
        <div class="discri text-left">
            <span id="cart-total"><?php echo $this->cart->countProducts(); ?></span>
        </div>
    </button>
    <ul class="dropdown-menu pull-right">
        <?php if ($products || $vouchers) { ?>
            <li>
                <div>
                    <?php foreach ($products as $product) { ?>
                        <div class="col-xs-12 cartdrop">
                            <div class="pull-left imgpro">
                                <?php if ($product['thumb']) { ?>
                                    <a href="<?php echo $product['href']; ?>">
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="pull-left cartname">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            </div>
                            <div class="pull-right">
                                <button type="button" onclick="cart.remove('40');" title="Remove" class="btn btn-pri btn-xs">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <div class="cartprice">
                                <span><?php echo $product['quantity']; ?> x</span>
                                <span><?php echo $product['price']; ?></span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php foreach ($vouchers as $voucher) { ?>
                        <div class="col-xs-12 cartdrop">
                            <div class="pull-left imgpro"></div>
                            <div class="pull-left cartname">
                                <a><?php echo $voucher['description']; ?></a>
                            </div>
                            <div class="pull-right">
                                <button type="button" onclick="cart.remove('40');" title="Remove" class="btn btn-pri btn-xs">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <div class="cartprice">
                                <span>1 x</span>
                                <span><?php echo $voucher['amount']; ?></span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </li>
            <li>
                <div>
                    <table class="table table-bordered">
                        <tbody>
                            <?php foreach ($totals as $total) { ?>
                                <tr>
                                    <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                                    <td class="text-right"><?php echo $total['text']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <p class="cartbt">
                        <a href="<?php echo $cart; ?>" class="btn btn-primary"><?php echo $text_cart; ?></a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $text_checkout; ?></a>
                    </p>
                </div>
            </li>
        <?php } else { ?>
            <li>
                <p class="text-center"><?php echo $text_empty; ?></p>
            </li>
        <?php } ?>
    </ul>
</div>