<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/cnc/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/cnc/');

// DIR
define('DIR_APPLICATION', 'G:/wamp/www/CNC/catalog/');
define('DIR_SYSTEM', 'G:/wamp/www/CNC/system/');
define('DIR_IMAGE', 'G:/wamp/www/CNC/image/');
define('DIR_STORAGE', 'G:/wamp/www/CNC/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'zn_opencart_cnc');
define('DB_PORT', '3306');
define('DB_PREFIX', 'ocjn_');