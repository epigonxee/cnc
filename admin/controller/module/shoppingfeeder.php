<?php
class ControllerModuleShoppingfeeder extends Controller {
    private $error = array();

    public function install()
    {
        //in 2.x we add events here
    }

    public function uninstall()
    {
        //in 2.x we add events here
    }

    public function index() {

        $this->load->language('module/shoppingfeeder');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('shoppingfeeder', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_edit'] = $this->language->get('text_edit');
        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');

        $this->data['apikey'] = $this->language->get('apikey');
        $this->data['apisecret'] = $this->language->get('apisecret');

        $this->data['help_apikey'] = $this->language->get('help_apikey');
        $this->data['help_apisecret'] = $this->language->get('help_apisecret');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['apikey'])) {
            $this->data['error_apikey'] = $this->error['apikey'];
        } else {
            $this->data['error_apikey'] = '';
        }

        if (isset($this->error['apisecret'])) {
            $this->data['error_apisecret'] = $this->error['apisecret'];
        } else {
            $this->data['error_apisecret'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/shoppingfeeder', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('module/shoppingfeeder', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['shoppingfeeder_apikey'])) {
            $this->data['shoppingfeeder_apikey'] = $this->request->post['shoppingfeeder_apikey'];
        } else {
            $this->data['shoppingfeeder_apikey'] = $this->config->get('shoppingfeeder_apikey');
        }

        if (isset($this->request->post['shoppingfeeder_apisecret'])) {
            $this->data['shoppingfeeder_apisecret'] = $this->request->post['shoppingfeeder_apisecret'];
        } else {
            $this->data['shoppingfeeder_apisecret'] = $this->config->get('shoppingfeeder_apisecret');
        }

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'module/shoppingfeeder.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/shoppingfeeder')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['shoppingfeeder_apikey']) {
            $this->error['apikey'] = $this->language->get('error_apikey');
        }

        if (!$this->request->post['shoppingfeeder_apisecret']) {
            $this->error['apisecret'] = $this->language->get('error_apisecret');
        }

        return !$this->error;
    }
}