<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="input-code"><span data-toggle="tooltip" data-html="true" data-trigger="click" title="<?php echo htmlspecialchars($help_apikey); ?>"><?php echo $apikey; ?></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="shoppingfeeder_apikey" rows="5" id="input-apikey" class="form-control" value="<?php echo $shoppingfeeder_apikey; ?>">
                        <?php if ($error_apikey) { ?>
                        <div class="text-danger"><?php echo $error_apikey; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="input-code"><span data-toggle="tooltip" data-html="true" data-trigger="click" title="<?php echo htmlspecialchars($help_apisecret); ?>"><?php echo $apisecret; ?></span></label>
                    <div class="col-sm-3">
                        <input type="text" name="shoppingfeeder_apisecret" rows="5" id="input-apikey" class="form-control" value="<?php echo $shoppingfeeder_apisecret; ?>">
                        <?php if ($error_apisecret) { ?>
                        <div class="text-danger"><?php echo $error_apisecret; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>