  $(document).ready(function(){
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'popup';
    //$.fn.editable.defaults.ajaxOptions = {type: "POST"};
    //make username editable
    $('.ocedit_quick').editable({
    error: function(response, newValue) {
    if(response.status === 401) {
      location.reload();
        return 'Not Authorized';
    } else {
        return response.responseText;
    }
   }
    });

/*
    $('.status_quick_edit').editable({
        source: [
              {value: 0, text: 'text_disabled'},
              {value: 1, text: 'text_enabled'}
           ]
    });
*/


		$(".cb-enable").click(function(){
			var parent = $(this).parents('.switch');
            var thisenable = $(this);

             $.post(thisenable.attr('data-url'), {name: "status", pk: thisenable.attr('data-pk'), value: thisenable.attr('data-value')}, function(data, textStatus, jqXHR){
             if(jqXHR.status!=200) {alert('error update status:'+jqXHR.status+'|'+textStatus);}
             else {
              $('.cb-disable',parent).removeClass('selected');
			  thisenable.addClass('selected');
             }
             }).fail(function() {  alert('Error. Not autorize'); });


		});
		$(".cb-disable").click(function(){
			var parent = $(this).parents('.switch');
            var thisdisable = $(this);
             $.post(thisdisable.attr('data-url'), {name: "status", pk: thisdisable.attr('data-pk'), value: thisdisable.attr('data-value')}, function(data, textStatus, jqXHR){
             if(jqXHR.status!=200) {alert('error update status:'+jqXHR.status+'|'+textStatus);}
             else {
		       $('.cb-enable',parent).removeClass('selected');
			   thisdisable.addClass('selected');
             }
             }).fail(function() {  alert('Error. Not autorize'); });

		});



});